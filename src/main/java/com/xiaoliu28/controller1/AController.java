package com.xiaoliu28.controller1;

import com.xiaoliu28.demos.web.User;
import io.swagger.annotations.*;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

@Controller
@Api(tags = "A管理模块", description = "A管理模块的API")
public class AController {
    private final ResourceLoader resourceLoader;

    public AController(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }
    @GetMapping("/a_get_wucanshu")
    @ApiOperation("a模块,get请求,无参数")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = String.class)
    })
    public ResponseEntity<String> aGetWuCan() {
        return ResponseEntity.ok("请求成功:a模块,get请求,无参数");
    }

    @GetMapping(value = "/a_get_pathduocanshu/{address}/{code}")
    @ApiOperation("a模块,get请求,path多参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "address", value = "地址", required = true, dataType = "string", paramType = "path"),
            @ApiImplicitParam(name = "code", value = "邮编", required = false, dataType = "string", paramType = "path")
    })
    @ResponseBody
    public ResponseEntity<String> aGetPathDuoCanShu(@PathVariable("address") String address, @PathVariable("code") String code) {
        System.out.println("address:" + address);
        System.out.println("code:" + code);
        return ResponseEntity.ok("请求成功:a模块,get请求,path多参数");
    }

    @GetMapping("/a_get_queryduocanshu")
    @ApiOperation("a模块,get请求,query多参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "姓名", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "age", value = "年龄", required = false, dataType = "string", paramType = "query")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = String.class)
    })
    public ResponseEntity<String> aGetQueryDuoCanShu(@RequestParam(name = "name", defaultValue = "小刘28") String name,@RequestParam(name = "age", defaultValue = "28") String age) {
        System.out.println("name:" + name);
        System.out.println("age:" + age);
        return ResponseEntity.ok("请求成功:a模块,get请求,query多参数");
    }

    @GetMapping("/a_get_pathqueryduocanshu/{address}/{code}")
    @ApiOperation("a模块,get请求,path和query多参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "address", value = "地址", required = true, dataType = "string", paramType = "path"),
            @ApiImplicitParam(name = "code", value = "邮编", required = false, dataType = "string", paramType = "path"),
            @ApiImplicitParam(name = "name", value = "姓名", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "age", value = "年龄", required = false, dataType = "string", paramType = "query")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = String.class)
    })
    public ResponseEntity<String> aGetPathQueryDuoCanShu(@PathVariable("address") String address, @PathVariable("code") String code,@RequestParam(name = "name", defaultValue = "小刘28") String name,@RequestParam(name = "age", defaultValue = "28") String age) {
        System.out.println("address:" + address);
        System.out.println("code:" + code);
        System.out.println("name:" + name);
        System.out.println("age:" + age);
        return ResponseEntity.ok("请求成功:a模块,get请求,path和query多参数");
    }

    @PostMapping("/a_post_wucanshu")
    @ApiOperation("a模块,post请求,无参数")
    @ResponseBody
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = String.class)
    })
    public ResponseEntity<String> aPostWuCan() {
        return ResponseEntity.ok("请求成功:a模块,post请求,无参数");
    }

    @PostMapping(value = "/a_post_pathduocanshu/{address}/{code}")
    @ApiOperation("a模块,post请求,path多参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "address", value = "地址", required = true, dataType = "string", paramType = "path"),
            @ApiImplicitParam(name = "code", value = "邮编", required = false, dataType = "string", paramType = "path")
    })
    @ResponseBody
    public ResponseEntity<String> aPostPathDuoCanShu(@PathVariable("address") String address, @PathVariable("code") String code) {
        System.out.println("address:" + address);
        System.out.println("code:" + code);
        return ResponseEntity.ok("请求成功:a模块,post请求,path多参数");
    }

    @PostMapping("/a_post_queryduocanshu")
    @ApiOperation("a模块,post请求,query多参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "姓名", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "age", value = "年龄", required = false, dataType = "string", paramType = "query")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = String.class)
    })
    public ResponseEntity<String> aPostQueryDuoCanShu(@RequestParam(name = "name", defaultValue = "小刘28") String name,@RequestParam(name = "age", defaultValue = "28") String age) {
        System.out.println("name:" + name);
        System.out.println("age:" + age);
        return ResponseEntity.ok("请求成功:a模块,post请求,query多参数");
    }

    @PostMapping("/a_post_bodyduocanshu")
    @ApiOperation("a模块,post请求,body多参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "requBody", value = "body请求体", required = true, dataType = "RequBody", paramType = "body"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = String.class)
    })
    public ResponseEntity<String> aPostBodyDuoCanShu(@RequestBody RequBody requBody) {
        Admin admin = requBody.getAdmin();
        VeriCode veriCode = requBody.getVeriCode();
        System.out.println("admin.username:" + admin.getUsername());
        System.out.println("admin.password:" + admin.getPassword());
        System.out.println("veriCode:" + veriCode.getCode());
        return ResponseEntity.ok("请求成功:a模块,post请求,body多参数");
    }

    @ApiOperation(value = "a模块,post请求,form多参数", consumes = "multipart/form-data")
    @PostMapping("/a_post_formduocanshu")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", value = "上传文件/上传文件数组", required = false, dataType = "__file", paramType = "form"),
            @ApiImplicitParam(name = "name", value = "姓名", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "age", value = "年龄", required = true, dataType = "string", paramType = "query"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = String.class)
    })
    // 注意:
    // 1.swagger-ui2的版本不支持同时上传多个文件调试,可以使用postman接口调试功能测试,是可以正常上传多个文件的
    // 2.不要上传太大的文件,否则会上传失败,这里不做解释,请自行百度解决
    public ResponseEntity<String> aPostFormDuoCanShu(@RequestParam("file") MultipartFile file,@RequestParam("name") String name,@RequestParam("age") String age) {
//        System.out.println("上传文件个数:" + file.length);
        System.out.println("文件名:" + file.getName());
        System.out.println("name:" + name);
        System.out.println("age:" + age);
        return ResponseEntity.ok("请求成功:a模块,post请求,form多参数");
    }

    @ApiOperation(value = "a模块,post请求,结果json")
    @PostMapping("/a_post_jieguojson")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = RespJSON.class)
    })
    public ResponseEntity<RespJSON> aPostJieGuoJSON() {
        RespJSON respJSON = new RespJSON("200","成功","我是返回的内容");
        return ResponseEntity.ok(respJSON);
    }



    @GetMapping("/a_get_xiazai")
    @ApiOperation(value = "a模块,get请求,下载", notes = "前端页面点击下载操作时调用接口",produces = "application/octet-stream")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = String.class)
    })
    public ResponseEntity<byte[]> aGetXiaZai() throws IOException {

        String fileName = "测试文件.txt"; // 指定文件名

        Resource resource = resourceLoader.getResource("classpath:static/" + fileName);
        System.out.println(fileName);

        try (InputStream inputStream = resource.getInputStream()) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            byte[] fileBytes = outputStream.toByteArray();

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            // 对文件名进行 URL 编码，避免文件名被浏览器编码成下划线
            String encodedFileName = UriUtils.encode(fileName, StandardCharsets.UTF_8);
            headers.setContentDispositionFormData("attachment", encodedFileName);

            return new ResponseEntity<>(fileBytes, headers, HttpStatus.OK);
        }
    }

}
