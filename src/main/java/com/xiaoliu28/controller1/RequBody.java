package com.xiaoliu28.controller1;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "请求体类")
public class RequBody {
    @ApiModelProperty(value = "账号类")
    private Admin admin;
    @ApiModelProperty(value = "验证类")
    private VeriCode veriCode;

    public RequBody() {
    }

    /**
     * 获取
     * @return admin
     */
    public Admin getAdmin() {
        return admin;
    }

    /**
     * 设置
     * @param admin
     */
    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    /**
     * 获取
     * @return veriCode
     */
    public VeriCode getVeriCode() {
        return veriCode;
    }

    /**
     * 设置
     * @param veriCode
     */
    public void setVeriCode(VeriCode veriCode) {
        this.veriCode = veriCode;
    }

    public String toString() {
        return "RequBody{admin = " + admin + ", veriCode = " + veriCode + "}";
    }
}
