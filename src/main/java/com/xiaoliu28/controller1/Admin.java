package com.xiaoliu28.controller1;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "账户类")
public class Admin {
    @ApiModelProperty(value = "账号,字符串,长度8-20",required = true)
    private String username;
    @ApiModelProperty(value = "密码,字符串,长度8-20",required = true)
    private String password;

    public Admin() {
    }

    /**
     * 获取
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 获取
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public String toString() {
        return "Admin{username = " + username + ", password = " + password + "}";
    }
}
