package com.xiaoliu28.controller1;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "返回体类")
public class RespJSON {
    @ApiModelProperty(value = "状态码")
    private String code;
    @ApiModelProperty(value = "提示")
    private String message;
    @ApiModelProperty(value = "返回数据")
    private Object data;

    public RespJSON() {
    }

    public RespJSON(String code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 获取
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 设置
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 获取
     * @return data
     */
    public Object getData() {
        return data;
    }

    /**
     * 设置
     * @param data
     */
    public void setData(Object data) {
        this.data = data;
    }

    public String toString() {
        return "RespJSON{code = " + code + ", message = " + message + ", data = " + data + "}";
    }
}
