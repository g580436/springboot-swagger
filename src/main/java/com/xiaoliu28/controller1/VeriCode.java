package com.xiaoliu28.controller1;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "验证码类")
public class VeriCode {
    @ApiModelProperty(value = "验证码,字符串,长度4",required = true)
    private String code;

    public VeriCode() {
    }

    /**
     * 获取
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    public String toString() {
        return "VeriCode{code = " + code + "}";
    }
}
