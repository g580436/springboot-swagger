package com.xiaoliu28.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    // 创建API1的规范文件
    @Bean
    public Docket api1() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .groupName("API 1")
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo("API 1", "1.0.0"))
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.xiaoliu28.controller1"))
                .paths(PathSelectors.any())
                .build();
    }

    // 创建API2的规范文件
    @Bean
    public Docket api2() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .groupName("API 2")
                .apiInfo(apiInfo("API 2", "1.0.0"))
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.xiaoliu28.controller2"))
                .paths(PathSelectors.any())
                .build();
    }

    // 设置API信息
    private ApiInfo apiInfo(String title, String version) {
        return new ApiInfoBuilder()
                .title(title)
                .version(version)
                .build();
    }
}
